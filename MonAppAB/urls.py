from django.urls import path
from . import views  
   urlpatterns = [
    path('articles', views.get_article, name='all_articles'),
    path('Article/<str:store_name>', views.create_article, name='article_create')
]