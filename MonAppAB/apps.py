from django.apps import AppConfig


class MonappabConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'MonAppAB'
