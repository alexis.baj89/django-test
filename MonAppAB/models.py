from django.db import models


# Create your models here.
class Article(models.Model):
    Nom_ART = models.CharField(max_length=200)
    Quantite = models.IntegerField(default=0)
    Prix_Unit = models.IntegerField(default=0)

    def __str__(self):
        return self.Nom_ART
