# Generated by Django 4.1.4 on 2022-12-20 10:12

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nom_ART', models.CharField(max_length=200)),
                ('Quantite', models.IntegerField(default=0)),
                ('Prix_Unit', models.IntegerField(default=0)),
            ],
        ),
    ]
