from django.test import TestCase
from .models import Article


# Create your tests here.
class ArticleTestCase(TestCase): 
    def setUp(self):
        Article.objects.create(nom_ART="mandarine")

    def test_one_article(self): 
        result = Article.objects.all()
        self.assertEqual(1, len(result))
        self.assertEqual('mandarine', result[0].nom_ART)